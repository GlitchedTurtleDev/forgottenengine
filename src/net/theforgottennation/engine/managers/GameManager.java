package net.theforgottennation.engine.managers;

import java.util.ArrayList;
import java.util.Arrays;

import net.forgottennation.core.Core;
import net.forgottennation.core.listeners.FListener;
import net.forgottennation.core.player.FPlayer;
import net.forgottennation.core.player.punishment.PunishmentGUI;
import net.forgottennation.core.update.UpdateEvent;
import net.forgottennation.core.update.UpdateRate;
import net.forgottennation.core.utils.C;
import net.forgottennation.core.utils.UtilPrefix;
import net.md_5.bungee.api.ChatColor;
import net.theforgottennation.engine.Engine;
import net.theforgottennation.engine.game.GamePlayer;
import net.theforgottennation.engine.game.GameType;
import net.theforgottennation.engine.game.PlayerType;
import net.theforgottennation.engine.map.MapManager;
import net.theforgottennation.engine.teams.GameTeam;
import net.theforgottennation.engine.utils.GameSettings;
import net.theforgottennation.engine.utils.GameState;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

public class GameManager extends FListener{
	private GameState state;
	private GameType activeGame;
	private ArrayList<GamePlayer> players = new ArrayList<GamePlayer>();
	int timer = GameSettings.LOBBY_TIME;
	public GameManager(GameType game)
	{
		activeGame = game;
		this.register();
		state = GameState.RESET;
	}
	
	public GameType getGame()
	{
		return activeGame;
	}
	
	public ArrayList<GamePlayer> getPlayers()
	{
		return players;
	}
	
	
	public void removePlayer(GamePlayer p)
	{
		players.remove(p);
	}
	public void setActiveGame(GameType activeGame) {
		this.activeGame = activeGame;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void join(PlayerJoinEvent event)
	{
		if(Bukkit.getOnlinePlayers().size() > getGame().getInstance().getPreventionSet().maxPlayers)
		{
			event.getPlayer().kickPlayer(ChatColor.GOLD + "TheRift\n" + ChatColor.RESET + "This server is currently full, please try again later");
			return;
		}
		
		if(state == GameState.INGAME || state == GameState.PREGAME){
			players.add(new GamePlayer(FPlayer.getPlayer(event.getPlayer()), PlayerType.SPECTATOR));
			event.getPlayer().teleport(GameSettings.LOBBY_SPWAN);
		} else {
			event.getPlayer().teleport(GameSettings.LOBBY_SPWAN);
			Player p = event.getPlayer();
			p.teleport(GameSettings.LOBBY_SPWAN);
			for(Player pl : Bukkit.getOnlinePlayers()){
				if(p == pl) continue;
				p.showPlayer(pl);
			}
			p.setHealth(20);
			p.setFoodLevel(20);
			p.getInventory().clear();
			p.getInventory().setArmorContents(new ItemStack[p.getInventory().getArmorContents().length]);
			p.setMaxHealth(20);
			p.setAllowFlight(false);
			p.setFlying(false);
			for(PotionEffect potion : p.getActivePotionEffects())
			{
				p.removePotionEffect(potion.getType());
			}
			
			p.getInventory().setItem(0, PunishmentGUI.getItem(ChatColor.GOLD + "Select Kit", Material.COMPASS,
					Arrays.asList(ChatColor.GRAY + "Use this item to select your kit")));
			p.getInventory().setItem(4, PunishmentGUI.getItem(ChatColor.LIGHT_PURPLE + "Funbox", Material.CHEST,
					Arrays.asList(ChatColor.GRAY + "A crate full of goodies!")));
			p.getInventory().setItem(8, PunishmentGUI.getItem(ChatColor.BLUE + "Return To Hub...", Material.BED,
					Arrays.asList(ChatColor.GRAY + "Return to the hub.")));
		}
		StatusManager.updatePlayersOnline();
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void quit(PlayerQuitEvent event)
	{
		FPlayer p = FPlayer.getPlayer(event.getPlayer().getUniqueId());
		if(getState() == GameState.INGAME || getState() == GameState.PREGAME) activeGame.getInstance().removePlayer(p);
	}
	
	@EventHandler
	public void q(PlayerQuitEvent event)
	{
		new BukkitRunnable() {
			
			@Override
			public void run() {
				StatusManager.updatePlayersOnline();
				
			}
		}.runTaskLater(Core.getInstance(), 5);
	}
	
	public int getTimer() {
		return timer;
	}
	
	//
	//	WARNING: RETURNS DEFAULT TEAM IF TEAM IS NOT FOUND.
	//
	public GameTeam getTeam(String name)
	{
		for(GameTeam t : activeGame.getInstance().getTeams())
		{
			if(t.name.equalsIgnoreCase( name )) return t;
		}
		return activeGame.getInstance().getTeams().get(0);
	}
	

	@EventHandler
	public void onUpdate(UpdateEvent event)
	{
		Engine.getInstance().getVanishManager().updateVanish();
		Engine.getInstance().getScoreboardManager().updateSidebar();
		if(event.getUpdateType() != UpdateRate.SECOND) return;
		switch(state)
		{
			case INGAME:
				activeGame.getInstance().onGameSecond();
				if(activeGame.getInstance().getPreventionSet().timeLimit)
				{
					timer--;
					if(timer <= 0)
					{
						if(activeGame.getInstance().getPreventionSet().deathmatch)
						{
							if(!activeGame.getInstance().hasDeathmatch()){
							activeGame.getInstance().startDeathmatch();
							}
							timer++;
						}
						activeGame.getInstance().onTimeLimitExceed();
					}
				}
				break;
			case LOBBY:
				timer--;
				if(Bukkit.getOnlinePlayers().size() < activeGame.getInstance().getPreventionSet().minPlayers){
					timer = GameSettings.LOBBY_TIME;
					break;
				}
				for(Player p : Bukkit.getOnlinePlayers())
				{
					if(timer % 5 == 0) p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
					if(timer <= 5) p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
					p.setLevel(timer);
				}
				if(timer % 5 == 0) Bukkit.broadcastMessage(C.gamePrefix + "The game will begin in " + timer + " seconds");
				if(timer <= 5) Bukkit.broadcastMessage(C.gamePrefix + "The game will begin in " + timer + " seconds");
				if(timer <= 0)
				{
					timer = GameSettings.PREGAME_TIME;
					setState(GameState.PREGAME);
					startPreGame();
				}
				
				break;
			case PREGAME:
				timer--;
				for(Player p : Bukkit.getOnlinePlayers())
				{
					p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
					p.setLevel(timer);
				}
				if(timer <= 0)
				{
					timer = activeGame.getInstance().getPreventionSet().timeLimitSec;
					setState(GameState.INGAME);
					startGame ();
				}
				break;
			case POSTGAME:
				timer--;
				if(timer <= 0)
				{
					resetGame();
				}
				break;
			case RESET:
				resetGame();
				break;
			default:
				break;
			case STOPPED:
				break;
		
		}
	}
	
	public void setState(GameState state) {
		this.state = state;
		StatusManager.updateGameState();
	}

	public GameState getState() {
		return state;
	}
	
	// Called when creating and teleporting players
	
	public void setupGame()
	{
		for(GameTeam gt : activeGame.getInstance().getTeams())
		{
			int i = 0;
			for(Player p : gt.getPlayers())
			{
				p.teleport(gt.spawns.get(i));
				i++;
			}
		}
	}
	
	
	public void startPreGame()
	{
	
		int i = 0;
		for(Player p : Bukkit.getOnlinePlayers())
		{
			if(i >= getGame().getInstance().getPreventionSet().maxPlayers)
			{
				p.sendMessage(ChatColor.RED + "You sit this game out, because it is full");
				players.add(new GamePlayer(FPlayer.getPlayer(p), PlayerType.SPECTATOR));
				continue;
			}
			p.playSound(p.getLocation(), Sound.LEVEL_UP, 0, 0);
			players.add(new GamePlayer(FPlayer.getPlayer(p), PlayerType.ALIVE));
			i++;
			
		}
		activeGame.getInstance().assignTeams();
		setupGame();
		for(i= 0; i < 100 ; i++) Bukkit.broadcastMessage(" ");
		Bukkit.broadcastMessage(C.gamePrefix + "Pre Game has Started");
		
		for(Player p : Bukkit.getOnlinePlayers())
		{
			if(Engine.getInstance().getKitManager().getKit(p) == null)
			{
				Engine.getInstance().getKitManager().AddKit(p, activeGame.getInstance().getKits().get(0));
			}
		}
		
		for(Player p : Bukkit.getOnlinePlayers())
		{
			p.getInventory().clear();
			Engine.getInstance().getKitManager().getKit(p).GiveItems(p);
			Engine.getInstance().getKitManager().getKit(p).register();
		}
		
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "===============================");
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + " ");
		Bukkit.broadcastMessage(UtilPrefix.getPrefix("Game") + activeGame.getName());
		for(String desc : activeGame.getDesc())
		{
			Bukkit.broadcastMessage(ChatColor.GRAY + "" +desc );
		}
		Bukkit.broadcastMessage(" ");
		for(Player p : Bukkit.getOnlinePlayers())
		{
			p.sendMessage(UtilPrefix.getPrefix("Kit") + Engine.getInstance().getKitManager().getKit(p).getName());
		}
		Bukkit.broadcastMessage(UtilPrefix.getPrefix("Map") + MapManager.currentMap.MapName + " by " + MapManager.currentMap.MapAuthor);
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "===============================");
		
		Bukkit.broadcastMessage(" ");
		
		activeGame.getInstance().onPreGameStart();
	}
	
	public void startGame()
	{
		activeGame.getInstance().registerListeners();
		Bukkit.broadcastMessage(C.gamePrefix + "The Game has Started");
		activeGame.getInstance().onGameStart();
		Engine.getInstance().hydraManage(true);
	}
	
	public void stopGame()
	{
		setState(GameState.POSTGAME);
		timer = 6;
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e)
	{
		if(getState() == GameState.LOBBY || getState() == GameState.STOPPED)
		{
			e.setCancelled(true);
		}
	}
	
	public void resetGame()
	{
		Engine.getInstance().hydraManage(false);
		setState(GameState.LOBBY);
		timer = GameSettings.LOBBY_TIME;
		MapManager.UnloadMap();
		Bukkit.broadcastMessage(C.gamePrefix + "The Game Server has been Reset!");
		for(Player p : Bukkit.getOnlinePlayers())
		{
			p.teleport(GameSettings.LOBBY_SPWAN);
			for(Player pl : Bukkit.getOnlinePlayers()){
				if(p == pl) continue;
				p.showPlayer(pl);
			}
			p.setHealth(20);
			p.setFoodLevel(20);
			p.getInventory().clear();
			p.setMaxHealth(20);
			p.setAllowFlight(false);
			p.setFlying(false);
			p.setCanPickupItems(false);
			
			p.getInventory().setArmorContents(new ItemStack[p.getInventory().getArmorContents().length]);
			for(PotionEffect potion : p.getActivePotionEffects())
			{
				p.removePotionEffect(potion.getType());
			}
			if(Engine.getInstance().getKitManager().getKit(p) != null)
				Engine.getInstance().getKitManager().getKit(p).unregister();
			p.getInventory().setItem(0, PunishmentGUI.getItem(ChatColor.GOLD + "Select Kit", Material.COMPASS,
					Arrays.asList(ChatColor.GRAY + "Use this item to select your kit")));
			p.getInventory().setItem(4, PunishmentGUI.getItem(ChatColor.LIGHT_PURPLE + "Funbox", Material.CHEST,
					Arrays.asList(ChatColor.GRAY + "A crate full of goodies!")));
			p.getInventory().setItem(8, PunishmentGUI.getItem(ChatColor.BLUE + "Return To Hub...", Material.BED,
					Arrays.asList(ChatColor.GRAY + "Return to the hub.")));
			
		}
		for(World w : Bukkit.getWorlds())
		{
			if(w == GameSettings.LOBBY_SPWAN.getWorld()) continue;
			for(Entity e : w.getEntities())
			{
				e.remove();
			}
		}
		Engine.getInstance().getKitManager().ClearKits();
		players.clear();
		MapManager.SelectMap();
	}

	public void setTimer(int timer) {
		this.timer = timer;
	}
}
