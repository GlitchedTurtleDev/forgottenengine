package net.theforgottennation.engine.managers;

public class CoinReason {
	private String reason;
	private int reward;
	private int xp;
	public CoinReason(String reason, int reward, int xp) {
		super();
		this.reason = reason;
		this.reward = reward;
		this.xp = xp;
	}
	public String getReason() {
		return reason;
	}
	public int getReward() {
		return reward;
	}
	public int getXP() {
		return xp;
	}
	
	
}
