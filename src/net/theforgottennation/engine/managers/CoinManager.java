package net.theforgottennation.engine.managers;

import java.util.List;

import net.forgottennation.core.chat.ChatControl;
import net.forgottennation.core.player.FPlayer;
import net.forgottennation.core.shop.ShopItem;
import net.forgottennation.core.utils.UtilPrefix;
import net.theforgottennation.engine.utils.GameSettings;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CoinManager {
	
	public static void GiveMoney(Player p, List<CoinReason> reasons)
	{
		p.sendMessage(" ");
		p.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "REWARDS");
		p.sendMessage(" "); 
		int coins = 0;
		long xp = 0;
		for(CoinReason rs : reasons)
		{
			p.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + rs.getReason() + " " + ChatColor.GOLD + "" + ChatColor.BOLD + "+" + rs.getReward() + " COINS " + ChatColor.AQUA + "" + ChatColor.BOLD + "+" + rs.getXP() + " XP      " );
			coins += rs.getReward();
			xp += rs.getXP();
			p.getWorld().playSound(p.getLocation(), Sound.FIREWORK_BLAST, 1, 1);
		}
		
		/*p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "+" + coins + " BAL" + ChatColor.AQUA + "" + ChatColor.BOLD + "+" + xp + " XP      " + ChatColor.GREEN + "" + ChatColor.BOLD + "Beta Tester");
		
		coins *= 2;
		xp *= 2;*/
		
		
		p.sendMessage(
				ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "TOTAL: " + ChatColor.GOLD + "" + ChatColor.BOLD + "+" + coins + " COINS  " + 
		ChatColor.AQUA + "" + ChatColor.BOLD + "+" + xp + " XP      "
						+ ChatColor.GREEN + "" + /*ChatColor.BOLD + Core.getInstance().bm.getBoosterForServer("ALPHA-1"));*/  "+0");
		p.getWorld().playSound(p.getLocation(), Sound.FIREWORK_TWINKLE, 1, 1);
		FPlayer.getPlayer(p).addBalance(coins);
		FPlayer.getPlayer(p).addXP(xp);
		ChatControl.freeze(5000);
	}
	
	public static void GiveLoot(Player p)
	{

		if(Math.random() < 0.1)
		{
			Bukkit.broadcastMessage(UtilPrefix.getPrefix("Treasure") + ChatColor.YELLOW + p.getDisplayName() + ChatColor.RESET + " has found a " + ChatColor.LIGHT_PURPLE + "Rare Mystical Chest");
			
			new ShopItem("Mystical Chest", p, 0).confirmPurchase(1, true);
		} else {
			p.sendMessage(UtilPrefix.getPrefix("Treasure") + "You got nothing because you are bad");
		} 
		if(Math.random() <= GameSettings.LOOT_DROP_CHANCE)
		{
			
		}
	}
}
