package net.theforgottennation.engine.managers;

import java.util.HashMap;
import java.util.UUID;

import net.theforgottennation.engine.Engine;
import net.theforgottennation.engine.game.GamePlayer;
import net.theforgottennation.engine.game.PlayerType;
import net.theforgottennation.engine.utils.GameState;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class VanishManager {
	private HashMap<UUID, Long> vanished = new HashMap<>();
	
	public void vanish(Player p , long time)
	{
		vanished.put(p.getUniqueId(), System.currentTimeMillis() + time);
	}
	
	public void updateVanish()
	{
		if(Engine.getInstance().getGameManager().getState() != GameState.INGAME) return;
		for(GamePlayer p : Engine.getInstance().getGameManager().getPlayers())
		{
			if(p.type == PlayerType.ALIVE)
			{
				if(vanished.containsKey(p.player.getPlayer().getUniqueId()))
				{
					if(vanished.get(p.player.getPlayer().getUniqueId()) > System.currentTimeMillis())
					{
						for(Player pl : Bukkit.getOnlinePlayers())
						{
							if(p.player.getPlayer() == pl) continue;
							pl.hidePlayer(p.player.getPlayer().getPlayer());
						}
						return;
					}
				}
				for(Player pl : Bukkit.getOnlinePlayers())
				{
					if(p.player.getPlayer() == pl) continue;
					pl.showPlayer(p.player.getPlayer().getPlayer());
				}
				p.player.getPlayer().spigot().setCollidesWithEntities(true);
			} else {
				for(Player pl : Bukkit.getOnlinePlayers())
				{
					if(p.player.getPlayer() == pl) continue;
					pl.hidePlayer(p.player.getPlayer().getPlayer());
				}	
				p.player.getPlayer().getPlayer().setAllowFlight(true);
				//p.player.getPlayer().getPlayer().setFlying(true);
				p.player.getPlayer().getPlayer().setHealth(20);
				p.player.getPlayer().getPlayer().setHealthScale(20);
				p.player.getPlayer().spigot().setCollidesWithEntities(false);
			}
		}
	}
}
