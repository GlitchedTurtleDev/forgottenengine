package net.theforgottennation.engine.managers;


import net.forgottennation.core.Core;
import net.forgottennation.core.listeners.FListener;
import net.forgottennation.core.player.FPlayer;
import net.forgottennation.core.reigon.Region;
import net.theforgottennation.engine.Engine;
import net.theforgottennation.engine.game.PlayerType;
import net.theforgottennation.engine.utils.GameSettings;
import net.theforgottennation.engine.utils.GameState;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class MovementManager extends FListener{
	public MovementManager()
	{
		this.register();
	}
	
	@EventHandler
	public void moveItem(InventoryClickEvent event)
	{
		if(Engine.getInstance().getGameManager().getState() == GameState.LOBBY)
		{
			if(event.getWhoClicked().getGameMode() ==GameMode.CREATIVE) return;
			event.setCancelled(true);
		}
		if(Engine.getInstance().getGameManager().getState() == GameState.INGAME)
		{
			if(Engine.getInstance().getGameManager().getGame().getInstance().getGamePlayerByPlayer(FPlayer.getPlayer((Player) event.getWhoClicked())).get(0).type != PlayerType.ALIVE)
					{
						event.setCancelled(true);
					}
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event)
	{
		if(event.getItem() == null) return;
		if(event.getAction() == Action.RIGHT_CLICK_AIR ||
				event.getAction() == Action.RIGHT_CLICK_BLOCK)
		{
			if(Engine.getInstance().getGameManager().getState() == GameState.LOBBY)
			{
				if(event.getItem().getType() == Material.COMPASS)
				{
					event.getPlayer().chat("/kit");
				}
				if(event.getItem().getType() == Material.BED)
				{
					event.getPlayer().chat("/hub");
				}
				if(event.getItem().getType() == Material.CHEST)
				{
					//TODO: Cosmetics
				}
			}
		}
	}
	
	@EventHandler
	public void moveCheckParkour(PlayerMoveEvent event)
	{
		if(new Region(
				new Location(Bukkit.getWorld("GamesLobby"),561,16,-494),
				new Location(Bukkit.getWorld("GamesLobby"),564,11,-498)).isLocationInsideRegion(event.getTo()))	
				{
					Core.getInstance().getAchievementManager().getAchievement("Parkour Legend").complete(event.getPlayer());
				}
	}
	
	
	@EventHandler
	public void onTalk(PlayerInteractEvent ev)
	{
		if(ev.getAction() == Action.PHYSICAL)
	
		{
			if(Engine.getInstance().getGameManager().getState() == GameState.LOBBY ||
					Engine.getInstance().getGameManager().getState() == GameState.STOPPED)
			{
				ev.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent event)
	{
		
		
		if(!Engine.getInstance().getGameManager().getGame().getInstance().getPreventionSet().canMoveDuringPreGame)
		{
			if(Engine.getInstance().getGameManager().getState() == GameState.PREGAME){
			Location to = event.getFrom();
			to.setPitch(event.getTo().getPitch());
			to.setYaw(event.getTo().getYaw());
			to.setY(event.getTo().getY());
			event.setTo(to);
			}
		}
		if(Engine.getInstance().getGameManager().getState() == GameState.LOBBY ||
				Engine.getInstance().getGameManager().getState() == GameState.STOPPED)
		{
			if(event.getTo().getWorld() == GameSettings.LOBBY_SPWAN.getWorld())
			{
				if(event.getTo().distance(GameSettings.LOBBY_SPWAN) > 400)
				{
					event.setCancelled(true);
				}
			}
						
		}
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event)
	{
		if(Engine.getInstance().getGameManager().getState() != GameState.INGAME){
			event.setCancelled(true);
			return;
		}
	}
	
	@EventHandler
	public void onFoodLevel(FoodLevelChangeEvent event)
	{
		if(Engine.getInstance().getGameManager().getState() != GameState.INGAME) event.setFoodLevel(20);
	}
	
	
	@EventHandler
	public void onEntityDamagedByEntity(EntityDamageByEntityEvent event)
	{
		if(event.getEntity().getName().equalsIgnoreCase("SpookyHat"))
		{
			if(event.getDamager() instanceof Player)
			{
				Core.getInstance().getAchievementManager().getAchievement("Spooky!").complete(((Player ) event.getDamager()));
			}
		}
		if(Engine.getInstance().getGameManager().getState() != GameState.INGAME){
			event.setCancelled(true);
			return;
		}
		if(event.getDamager() instanceof Player && event.getEntity() instanceof Player)
		{
			if(!Engine.getInstance().getGameManager().getGame().getInstance().getPreventionSet().pvp) event.setCancelled(true);
		}
		if(event.getDamager() instanceof Player && !(event.getEntity() instanceof Player))
		{
			if(!Engine.getInstance().getGameManager().getGame().getInstance().getPreventionSet().pve) event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onSpawn(CreatureSpawnEvent event)
	{
		if(event.getSpawnReason() == SpawnReason.NATURAL)
		{
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent event)
	{
		if(Engine.getInstance().getGameManager().getState() != GameState.INGAME){
			if(event.getPlayer().getGameMode() == GameMode.CREATIVE) return;
			event.setCancelled(true);
			return;
		}
		if(Engine.getInstance().getGameManager().getGame().getInstance().getPreventionSet().breakBlock) return;
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent event)
	{
		if(Engine.getInstance().getGameManager().getState() != GameState.INGAME){
			if(event.getPlayer().getGameMode() == GameMode.CREATIVE) return;
			event.setCancelled(true);
			return;
		}
		if(Engine.getInstance().getGameManager().getGame().getInstance().getPreventionSet().placeBlock) return;
		event.setCancelled(true);
	}
}
