package net.theforgottennation.engine.commands;

import java.util.Arrays;

import net.forgottennation.core.commands.FCommand;
import net.forgottennation.core.player.Rank;
import net.forgottennation.core.utils.C;
import net.theforgottennation.engine.Engine;
import net.theforgottennation.engine.game.GamePlayer;
import net.theforgottennation.engine.game.GameType;
import net.theforgottennation.engine.utils.GameState;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;

public class GameCommand extends FCommand {

	public GameCommand() {
		super("game", Rank.DEVELOPER, Arrays.asList("g"));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(Player sender, String[] args) {
		if(args.length == 0) return;
		if(args[0].equalsIgnoreCase("pause")) Engine.getInstance().getGameManager().setState(GameState.STOPPED);
		if(args[0].equalsIgnoreCase("reset")) Engine.getInstance().getGameManager().resetGame();
		if(args[0].equalsIgnoreCase("start")){
			Engine.getInstance().getGameManager().resetGame();
			Engine.getInstance().getGameManager().setTimer(5);
		}
		
		if(args[0].equalsIgnoreCase("set"))
		{
			if(args.length != 2) return;
			Engine.getInstance().getGameManager().setActiveGame(GameType.valueOf(args[1].toUpperCase()));
			//Engine.getInstance()..setType(GameType.valueOf(args[1].toUpperCase()));
			sender.sendMessage(C.gamePrefix + "Set the game to " + GameType.valueOf(args[1]).getName());
		}
		
		if(args[0].equalsIgnoreCase("world"))
		{
			World w  = Bukkit.createWorld(new WorldCreator(args[1]));
			
			sender.teleport(w.getSpawnLocation());
		}
		
		if(args[0].equalsIgnoreCase("dump"))
		{
			for(GamePlayer p : Engine.getInstance().getGameManager().getPlayers())
			{
				Bukkit.broadcastMessage(p.player.getPlayer().getName() + ": " + p.type.toString());
			}
		}
	}
	
}
