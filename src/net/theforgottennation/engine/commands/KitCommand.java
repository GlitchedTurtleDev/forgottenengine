package net.theforgottennation.engine.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.forgottennation.core.commands.FCommand;
import net.forgottennation.core.player.FPlayer;
import net.forgottennation.core.player.Rank;
import net.forgottennation.core.player.punishment.PunishmentGUI;
import net.forgottennation.core.shop.ShopItem;
import net.forgottennation.core.utils.C;
import net.md_5.bungee.api.ChatColor;
import net.theforgottennation.engine.Engine;
import net.theforgottennation.engine.kit.IKitPurchaseable;
import net.theforgottennation.engine.kit.Kit;
import net.theforgottennation.engine.utils.GameState;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class KitCommand extends FCommand{

	public KitCommand() {
		super("kit", Rank.DEFAULT, Arrays.asList("kit"));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(Player sender, String[] args) {
		// TODO Auto-generated method stub
		if(Engine.getInstance().getGameManager().getState() == GameState.INGAME || Engine.getInstance().getGameManager().getState() == GameState.PREGAME)
		{
			sender.sendMessage(C.errorPrefix + "It seems that you are trying to change your kit in game. We do not allow this do we?");
			return;
		}
		
	
		Inventory inv = Bukkit.createInventory(null, 27, ChatColor.LIGHT_PURPLE + "Kit Selector");
		
		int i = 9;
		for(Kit k : Engine.getInstance().getGameManager().getGame().getInstance().getKits()){
			List<String> lore = new ArrayList<>();
			
			if(k instanceof IKitPurchaseable)
			{
				IKitPurchaseable kit = (IKitPurchaseable) k;
		
				lore.add(ChatColor.GRAY + "Unlocked with the " + kit.getPerm().getTag() + ChatColor.GRAY + " rank or " + kit.getCost() + " coins");
				lore.add(" ");
			
			}
			else {
				lore.add(ChatColor.GRAY + "Free kit");
				lore.add(" ");
			}
			k.getDesc().stream().forEach(desc -> {
				lore.add(ChatColor.WHITE + desc);
			});
			if(k instanceof IKitPurchaseable)
			{
				IKitPurchaseable kit = (IKitPurchaseable) k;
				lore.add(" ");
				if(new ShopItem(k.getName(), sender, kit.getCost()).isOwned()
						|| FPlayer.getPlayer((Player) sender).getRank().isPermissable(((IKitPurchaseable) kit).getPerm()))
				{
					lore.add(ChatColor.GREEN + "" + ChatColor.BOLD + "OWNED");
				} else {
					lore.add(ChatColor.RED + "" + ChatColor.BOLD + "UNOWNED");
				}
			}
			ItemStack it = PunishmentGUI.getItem(ChatColor.GOLD + k.getName(), k.getItem().getType(), lore);
			if(i % 9 == 0 || (i-1) % 9 == 0) i++;
			inv.setItem(i, it);
		
			i++;
		}
		
		sender.openInventory(inv);
	}
	
	
	
}
