package net.theforgottennation.engine.teams;

import java.util.ArrayList;

import net.theforgottennation.engine.Engine;
import net.theforgottennation.engine.game.GamePlayer;
import net.theforgottennation.engine.game.PlayerType;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class GameTeam {
	public String name;
	public ChatColor c;
	public ArrayList<Location> spawns = new ArrayList<Location>();
	public GameTeam(String name, ChatColor c)
	{
		this.c = c;
		this.name = name;
		
	}
	
	public int getAlivePlayers()
	{
		int alive = 0;
		for(GamePlayer p : Engine.getInstance().getGameManager().getGame().getInstance().getPlayersWithType(PlayerType.ALIVE))
		{
			if(p.team == this) alive++;
		}
		return alive;
	}
	
	public ChatColor getColor() 
	{
		return c;
	}
	
	public ArrayList<Player> getPlayers()
	{
		ArrayList<Player> alive = new ArrayList<>();
		for(GamePlayer p : Engine.getInstance().getGameManager().getGame().getInstance().getPlayers())
		{
			if(p.team == this) alive.add(p.player.getPlayer().getPlayer());
		}
		return alive;
	}
	
}
