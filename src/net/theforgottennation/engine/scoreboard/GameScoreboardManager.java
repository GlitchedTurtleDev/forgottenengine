package net.theforgottennation.engine.scoreboard;

import java.util.Arrays;
import java.util.List;

import net.forgottennation.core.Core;
import net.forgottennation.core.scoreboard.PlayerScoreboard;
import net.forgottennation.core.scoreboard.ScoreboardManager;
import net.md_5.bungee.api.ChatColor;
import net.theforgottennation.engine.Engine;
import net.theforgottennation.engine.game.GamePlayer;
import net.theforgottennation.engine.teams.GameTeam;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class GameScoreboardManager {
	public ScoreboardManager sm;
	
	List<String> title = Arrays.asList(
			
			ChatColor.GOLD + "" + ChatColor.BOLD + "The Rift Games",
			ChatColor.GOLD + "" + ChatColor.BOLD + "The Rift Game",
			ChatColor.GOLD + "" + ChatColor.BOLD + "The Rift Gam",
			ChatColor.GOLD + "" + ChatColor.BOLD + "The Rift Ga",
			ChatColor.GOLD + "" + ChatColor.BOLD + "The Rift G",
			ChatColor.GOLD + "" + ChatColor.BOLD + "The Rift ",
			ChatColor.GOLD + "" + ChatColor.BOLD + "The Rif ",
			ChatColor.GOLD + "" + ChatColor.BOLD + "The Ri ",
			ChatColor.GOLD + "" + ChatColor.BOLD + "The R ",
			ChatColor.GOLD + "" + ChatColor.BOLD + "The  ",
			ChatColor.GOLD + "" + ChatColor.BOLD + "Th",
			ChatColor.GOLD + "" + ChatColor.BOLD + "T",
			ChatColor.GOLD + "" + ChatColor.BOLD + "", 
			ChatColor.BLUE + "" + ChatColor.BOLD + "t",
			ChatColor.BLUE + "" + ChatColor.BOLD + "th",
			ChatColor.BLUE + "" + ChatColor.BOLD + "the",
			ChatColor.BLUE + "" + ChatColor.BOLD + "ther",
			ChatColor.BLUE + "" + ChatColor.BOLD + "theri",
			ChatColor.BLUE + "" + ChatColor.BOLD + "therif",
			ChatColor.BLUE + "" + ChatColor.BOLD + "therift",
			ChatColor.BLUE + "" + ChatColor.BOLD + "therift.",
			ChatColor.BLUE + "" + ChatColor.BOLD + "therfit.t",
			ChatColor.BLUE + "" + ChatColor.BOLD + "therift.tk"
			);
	
	int clock = 0;
	
	int hold = -1;
	
	enum MoveType {
		FORWARD,
		BACKWARDS,
		HOLD
	}
	
	MoveType type = MoveType.FORWARD;
	
	public GameScoreboardManager()
	{
		sm = Core.getInstance().getScoreboardManager();
		
		if(sm == null) return;
		
	}
	
	public void showGameTeams(Player p)
	{
		Scoreboard s = p.getScoreboard();
		for(GameTeam t : Engine.getInstance().getGameManager().getGame().getInstance().getTeams())
		{
			Team team;
			if(s.getTeam(t.name) == null){
			team = s.registerNewTeam(t.name);
			team.setCanSeeFriendlyInvisibles(true);
			team.setAllowFriendlyFire(!Engine.getInstance().getGameManager().getGame().getInstance().getPreventionSet().canDamageTeam);
			
			team.setPrefix(t.c + "");
			} else {
				team = s.getTeam(t.name);
			}
			
			for(GamePlayer gp : Engine.getInstance().getGameManager().getGame().getInstance().getPlayers())
			{
				
				if(gp.team == t)
				{
					if(!team.hasEntry(gp.player.getPlayer().getName()))
						team.addEntry(gp.player.getPlayer().getName());
				}
			}
		}
	}
	
	public void showOriginalTeams()
	{
		Core.getInstance().getScoreboardManager().updateTags();
	}
	
	public void updateSidebar()
	{
		if(type ==MoveType.FORWARD)
		{
			clock++;
		} else if (type == MoveType.BACKWARDS){
			clock--;
		}
		if(clock == 0)
		{
			type = MoveType.HOLD;
			clock = 0;
			if(hold == -1)
			{
				hold = 100;
			}
			if(hold > 0)
			{
				hold--;
			} else {
				clock++;
				hold = -1;
			
				type = MoveType.FORWARD;
			}
		}
		if(clock == title.size() - 1)
		{
			type = MoveType.HOLD;
			clock = title.size()-1;
			if(hold == -1)
			{
				hold = 50;
			}
			if(hold > 0)
			{
				hold--;
			} else {
				clock--;
				hold = -1;
				type = MoveType.BACKWARDS;
			}
		}
		for(PlayerScoreboard s : sm.scoreboards)
		{
			if(s.getOwner() == null) continue;
			if(s.getOwner().getPlayer() == null) continue;
			s.setTitle(title.get(clock));
			switch(Engine.getInstance().getGameManager().getState()){
			case LOBBY:
				showOriginalTeams();
			
				
				s.setLine(15, ChatColor.GOLD + "" + ChatColor.BOLD + "Game: ");
				s.setLine(14, Engine.getInstance().getGameManager().getGame().getName());
				s.setLine(13," ");
				
				s.setLine(12, ChatColor.GOLD + "" + ChatColor.BOLD + "Players: ");
				s.setLine(11,Bukkit.getOnlinePlayers().size() + "/" + Engine.getInstance().getGameManager().getGame().getInstance().getPreventionSet().maxPlayers);
			
				s.setLine(10,ChatColor.RESET + " ");
				
				s.setLine(9,ChatColor.GREEN + "" + ChatColor.BOLD + "Kit:");
				if(Engine.getInstance().getKitManager().getKit(s.getOwner().getPlayer().getPlayer()) != null) 
					s.setLine(8,Engine.getInstance().getKitManager().getKit(s.getOwner().getPlayer().getPlayer()).getName());
				else
					s.setLine(8,"None");
				s.setLine(7,ChatColor.COLOR_CHAR + " ");
				
				s.setLine(6,ChatColor.GOLD + "" + ChatColor.BOLD + "Balance:");
				s.setLine(5,s.getOwner().getBalance() + "");
				
				s.setLine(4,ChatColor.COLOR_CHAR + "7" + ChatColor.COLOR_CHAR + "c ");
				
				s.setLine(2, "therift.tk");
				
				s.setLine(1, ChatColor.BLACK + " ");
				
				
				if(Bukkit.getOnlinePlayers().size() >= Engine.getInstance().getGameManager().getGame().getInstance().getPreventionSet().minPlayers){
					s.setLine(0,ChatColor.GOLD + "" + ChatColor.BOLD + "Game starts in " + Engine.getInstance().getGameManager().getTimer());
				} else {
					s.setLine(0,ChatColor.GOLD + "" + ChatColor.BOLD + "Waiting for Players");
				}
				break;
			case INGAME:
				showGameTeams(s.getOwner().getPlayer().getPlayer());
				Engine.getInstance().getGameManager().getGame().getInstance().handleSidebar(s,s.getOwner().getPlayer().getPlayer());
				break;
			case PREGAME:
				showGameTeams(s.getOwner().getPlayer().getPlayer());
				Engine.getInstance().getGameManager().getGame().getInstance().handleSidebar(s,s.getOwner().getPlayer().getPlayer());
				break;
			case RESET:
				break;
			default:
				break;
			case STOPPED:
				showOriginalTeams();
			
				
				s.setLine(15, ChatColor.GOLD + "" + ChatColor.BOLD + "Game: ");
				s.setLine(14, Engine.getInstance().getGameManager().getGame().getName());
				s.setLine(13," ");
				
				s.setLine(12, ChatColor.GOLD + "" + ChatColor.BOLD + "Players: ");
				s.setLine(11,Bukkit.getOnlinePlayers().size() + "/" + Engine.getInstance().getGameManager().getGame().getInstance().getPreventionSet().maxPlayers);
			
				s.setLine(10,ChatColor.RESET + " ");
				
				s.setLine(9,ChatColor.GREEN + "" + ChatColor.BOLD + "Kit:");
				if(Engine.getInstance().getKitManager().getKit(s.getOwner().getPlayer().getPlayer()) != null) 
					s.setLine(8,Engine.getInstance().getKitManager().getKit(s.getOwner().getPlayer().getPlayer()).getName());
				else
					s.setLine(8,"None");
				s.setLine(7,ChatColor.COLOR_CHAR + " ");
				
				s.setLine(6,ChatColor.GOLD + "" + ChatColor.BOLD + "Balance:");
				s.setLine(5,s.getOwner().getBalance() + "");
				
				s.setLine(4,ChatColor.COLOR_CHAR + "7" + ChatColor.COLOR_CHAR + "c ");
				
				if(Bukkit.getOnlinePlayers().size() >= Engine.getInstance().getGameManager().getGame().getInstance().getPreventionSet().minPlayers){
					s.setLine(3,ChatColor.GOLD + "" + ChatColor.BOLD + "Game starts in " + Engine.getInstance().getGameManager().getTimer());
				} else {
					s.setLine(3,ChatColor.GOLD + "" + ChatColor.BOLD + "Waiting for Players");
				}
				break;
			}
			s.bakeScoreboard();
		}
	}
}
