package net.theforgottennation.engine;

import net.forgottennation.core.achievements.Achievement;
import net.theforgottennation.engine.commands.GameCommand;
import net.theforgottennation.engine.commands.HubCommand;
import net.theforgottennation.engine.commands.KitCommand;
import net.theforgottennation.engine.event.EventServer;
import net.theforgottennation.engine.game.GameType;
import net.theforgottennation.engine.kit.KitManager;
import net.theforgottennation.engine.managers.GameManager;
import net.theforgottennation.engine.managers.MovementManager;
import net.theforgottennation.engine.managers.VanishManager;
import net.theforgottennation.engine.map.MapManager;
import net.theforgottennation.engine.scoreboard.GameScoreboardManager;
import net.theforgottennation.engine.utils.GameSettings;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.plugin.java.JavaPlugin;

public class Engine extends JavaPlugin{
	//private EngineMySQL enginesql = new EngineMySQL();
	private GameManager gamemanage;
	private static Engine instance;
	private GameScoreboardManager gsm;
	private MapManager mapmanager;
	private KitManager km;
	private VanishManager vm;
	public void onDisable()
	{
		Bukkit.broadcastMessage("Disabled Plugin");
		mapmanager.currentMap.Uninitialize();
	}
	
	public KitManager getKitManager()
	{
		return km;
	}
	
	public void onEnable()
	{
		instance = this;
		Bukkit.getLogger().info("=============================");
		Bukkit.getLogger().info("Started Forgotten Engine");
		Bukkit.getLogger().info(" by GlitchedTurtle");
		Bukkit.getLogger().info("This Plugin is intellectual property of GlitchedTurtle.");
		Bukkit.getLogger().info("All rights are reserved to GlitchedTurtle and his associates");
		Bukkit.getLogger().info(" ");
		Bukkit.getLogger().info("Server PORT: " + Bukkit.getPort());
		
		
		
		World lobby = getServer().createWorld(new WorldCreator("GamesLobby"));
		 
		GameSettings.LOBBY_SPWAN = lobby.getBlockAt(-115, 128, -107).getLocation().add(0.5,3,0.5);
		
		if(false)
		{
			Bukkit.getLogger().info("Server has already been registered");
			//Bukkit.getLogger().info("Server Type: " + enginesql.getServerType(Bukkit.getPort()));
		} else 
		{
			Bukkit.getLogger().info("Server is not registered");
			//String reqname = null;
			//if( new File("name.txt").exists())
			//{
			//	try {
			//		Scanner s = new Scanner(new File("name.txt"));
			//		reqname = s.nextLine();
			//		s.close();
			//	} catch(Exception e){}
			//}
			//enginesql.createServer(Bukkit.getPort(), GameType.SKYWARS.toString(),reqname);
		}
		Bukkit.getLogger().info("Server Joinable: " + true);
		Bukkit.getLogger().info("=============================");
		
		gamemanage = new GameManager(GameType.SKYWARS);
		
		gsm = new GameScoreboardManager();
		
		mapmanager = new MapManager();
		
		new MovementManager();
		
		new KitCommand();
		new GameCommand();
		new HubCommand();
		
		new MapManager();
		
		new Achievement("Ice to meet you", "Kill an enemy with an Ice block", "Defend The Core", 3000);
		new Achievement("The Diamond Minecart", "Kill an enemy inside a minecart \nwith a diamond sword", "SkyWars", 5000);
		
		km = new KitManager();
		vm = new VanishManager();
		
		new EventServer();
	
	}
	
	
	public void hydraManage(boolean on)
	{
		/*
		if(Bukkit.getPluginManager().getPlugin("Hydra") != null)
		{
			if(on)
				Bukkit.getPluginManager().enablePlugin(Bukkit.getPluginManager().getPlugin("Hydra"));
			else
				Bukkit.getPluginManager().disablePlugin(Bukkit.getPluginManager().getPlugin("Hydra"));
		}*/
	}
	
	public VanishManager getVanishManager() {
		return vm;
	}

	public void setVm(VanishManager vm) {
		this.vm = vm;
	}
	public MapManager getMapManager() {
		return mapmanager;
	}

	public GameScoreboardManager getScoreboardManager() {
		return gsm;
	}

	public GameManager getGameManager()
	{
		return gamemanage;
	}
	
	public static Engine getInstance()
	{
		return instance;
	}
	
	
}
