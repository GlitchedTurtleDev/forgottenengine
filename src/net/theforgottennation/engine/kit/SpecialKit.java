package net.theforgottennation.engine.kit;

public interface SpecialKit {
	public boolean isHidden();
	public String getSecretName();
	public String getSecretDescription();
	public String getSecretCost();
}
