package net.theforgottennation.engine.kit;

import java.util.ArrayList;
import java.util.List;

import net.forgottennation.core.listeners.FListener;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class Kit extends FListener {
	private String name;
	public String getName() {
		return name;
	}

	public ItemStack getItem() {
		return item;
	}

	public List<String> getDesc() {
		return desc;
	}

	private ItemStack item;
	private List<String> desc;
	
	public abstract void GiveItems(Player p);
	
	public void SetItems(Player p)
	{
		p.getInventory().clear();
		GiveItems(p);
		p.updateInventory();
	}
	
	
	public Kit(String name, Material is, String[] desc)
	{
		this.name = name;
		this.item = new ItemStack(is);
		this.desc = new ArrayList<String>();
		for (String str : desc)
		{
			this.desc.add(str);
		}
	}

}
