package net.theforgottennation.engine.kit;

import java.util.HashMap;
import java.util.UUID;

import net.forgottennation.core.listeners.FListener;
import net.forgottennation.core.player.FPlayer;
import net.forgottennation.core.shop.ShopItem;
import net.forgottennation.core.utils.C;
import net.theforgottennation.engine.Engine;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;

public class KitManager extends FListener{
	public KitManager()
	{
		this.register();
	}
	
	private HashMap<UUID, Kit> kits = new HashMap<>();
	
	public void AddKit(Player p, Kit k)
	{
		p.sendMessage(C.gamePrefix + "Set kit to " + k.getName());
		kits.put(p.getUniqueId(), k);
	}
	public void RemoveKit(Player p)
	{
		kits.remove(p.getUniqueId());
	}
	
	public Kit getKit(Player p)
	{
		return kits.get(p.getUniqueId());
	}
	
	public void ClearKits()
	{
		kits.clear();
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent event)
	{
		if(event.getCurrentItem() == null) return;
		if(event.getInventory().getTitle() == null) return;
		if(!event.getInventory().getTitle().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "Kit Selector")) return;
		if(event.getCurrentItem().getType() == Material.AIR) return;
		if(!event.getCurrentItem().hasItemMeta()){
			event.setCancelled(true);
			return;
		}
		Kit kit = null;
		String kitName = ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName());
		for(Kit k : Engine.getInstance().getGameManager().getGame().getInstance().getKits())
		{
			if(kitName.equals(k.getName()))
			{
				kit = k;
				break;
			}
		}
		if(kit instanceof IKitPurchaseable)
		{
			int cost = ((IKitPurchaseable) kit).getCost();
			if(!new ShopItem(kit.getName(), (Player) event.getWhoClicked(), cost).isOwned()
					&& !FPlayer.getPlayer((Player) event.getWhoClicked()).getRank().isPermissable(((IKitPurchaseable) kit).getPerm())){
				new ShopItem(kit.getName(), (Player) event.getWhoClicked(), cost).purchaseItem();
				return;
			}
		
		}
		
		AddKit((Player) event.getWhoClicked(), kit);
		event.setCancelled(true);
		event.getWhoClicked().closeInventory();
	}
}
