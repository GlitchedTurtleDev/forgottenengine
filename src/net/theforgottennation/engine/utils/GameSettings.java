package net.theforgottennation.engine.utils;

import net.forgottennation.core.player.Rank;

import org.bukkit.Location;

public class GameSettings {
	public static int LOBBY_TIME = 30;
	public static int PREGAME_TIME = 10;
	public static Location LOBBY_SPWAN;
	public static int MYSTICAL_DROP_CHANCE = 1;
	public static int LOOT_DROP_CHANCE = 0;
}
