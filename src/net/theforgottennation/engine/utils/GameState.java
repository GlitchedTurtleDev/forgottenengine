package net.theforgottennation.engine.utils;

public enum GameState {
	LOBBY,
	PREGAME,
	INGAME,
	POSTGAME,
	RESET,
	STOPPED;
}
