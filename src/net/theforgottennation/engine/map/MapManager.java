package net.theforgottennation.engine.map;

import java.util.ArrayList;
import java.util.Random;

import net.forgottennation.core.listeners.FListener;
import net.forgottennation.core.utils.Debugger;
import net.theforgottennation.engine.Engine;
import net.theforgottennation.engine.teams.GameTeam;
import net.theforgottennation.engine.utils.GameSettings;
import net.theforgottennation.engine.utils.GameState;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class MapManager extends FListener{
	public MapManager() {
		super();
		// TODO Auto-generated constructor stub
		register();
	}

	public static WorldData currentMap;
	private static UnparsedWorld PreviousMap;
	public static void SelectMap()
	{
		ArrayList<UnparsedWorld> pool = new MapParser().getMapsForGame(Engine.getInstance().getGameManager().getGame());
		
		
		try {
		if(pool.size() == 0)
		{
			Debugger.warn("No maps found for this game");
			Engine.getInstance().getGameManager().setState(GameState.STOPPED);
			return;
		}
		if(pool.size() != 1){
		
			Bukkit.broadcastMessage(pool.size() + "");
			
			if(PreviousMap != null)	pool.remove(PreviousMap);
			
			currentMap = new WorldData(pool.get(new Random().nextInt(pool.size())).getZip(), Engine.getInstance().getGameManager().getGame());
		} else {
			
			currentMap = new WorldData(pool.get(0).getZip(), Engine.getInstance().getGameManager().getGame());
		}
		} catch(Exception e)
		{
			Debugger.fatal(e.getMessage());
		}
	}
	
	@EventHandler
	public void onMapLoad(MapLoadEvent e)
	{
		Debugger.log("Map loaded event called");
		Bukkit.broadcastMessage(ChatColor.GREEN + "Map> " + "Setting map to " + e.getMapName() + " by " + e.getMapAuthor());
		
		LoadSpawnLocations();
	}
	
	public static void LoadSpawnLocations()
	{
		ArrayList<GameTeam> activeTeams = Engine.getInstance().getGameManager().getGame().getInstance().getTeams();
		Debugger.log("loading locations");
		for (GameTeam t : activeTeams)
		{
			Debugger.log("team" + t.name);
			t.spawns.removeAll(t.spawns);
			for (String s : currentMap.SpawnLocs.keySet())
			{
				if(s.equals(t.name.toUpperCase()))
				{
					t.spawns.addAll(currentMap.SpawnLocs.get(s));
				}
			}
		}
	}

	public static void UnloadMap()
	{
		if(currentMap == null) return;
		for(Player p : currentMap.World.getPlayers())
		{
			p.teleport(GameSettings.LOBBY_SPWAN);
		}
		currentMap.Uninitialize();
	}
	
	public static WorldData getCurrentMap() {
		return currentMap;
	}

	public static UnparsedWorld getPreviousMap() {
		return PreviousMap;
	}
	
	
}
