package net.theforgottennation.engine.game;

public enum PlayerType {
	ALIVE, DEAD, SPECTATOR;
}
