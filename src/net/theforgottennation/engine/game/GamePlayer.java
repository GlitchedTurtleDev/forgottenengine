package net.theforgottennation.engine.game;

import java.util.UUID;

import net.forgottennation.core.player.FPlayer;
import net.theforgottennation.engine.teams.GameTeam;

public class GamePlayer {
	public int kills = 0;
	public int deaths = 0;
	public PlayerType type;
	public FPlayer player;
	public GameTeam team;
	
	
	private long lastDamage = 0;
	private UUID lastDamager;
	private String lastDamageWith;
	
	public GamePlayer(FPlayer player, PlayerType type)
	{
		this.type = type;
		this.player = player;
	}
	
	public void revokeDamage() { lastDamage = 0; }
	
	public void damage(UUID p, String weapon)
	{
		lastDamage = System.currentTimeMillis();
		lastDamager = p;
		lastDamageWith = weapon;
	}

	public UUID getLastDamager()
	{
		return lastDamager;
	}
	
	public String getLastDamageWith()
	{
		return lastDamageWith;
	}
}