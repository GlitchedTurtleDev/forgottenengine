package net.theforgottennation.engine.game;

public class PreventionSet {
	public boolean canMoveDuringPreGame = false;
	public boolean canDamageTeam = false;
	public boolean waterDamage = false;
	public boolean pvp = true;
	public boolean pve = false;
	public boolean deathmatch = false;
	public boolean placeBlock = false;
	public boolean respawn = false;
	public boolean breakBlock = false;
	public boolean timeLimit = false;
	public boolean teamGame = false;
	public boolean handleDeath = true;
	public boolean dropItems = false;
	public int timeLimitSec = 120;
	public int maxPlayers = 20;
	public int minPlayers = 3;
}
