package net.theforgottennation.engine.game.type.sg;

import net.theforgottennation.engine.kit.Kit;
import net.theforgottennation.engine.kit.SpecialKit;

import org.bukkit.Material;
import org.bukkit.entity.Player;

public class SecretKit extends Kit implements SpecialKit {

	public SecretKit() {
		super("Special Kit", Material.DIAMOND, new String[]{"Secret kit for swags only"});
		// TODO Auto-generated constructor stub
	}

	@Override
	public void GiveItems(Player p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isHidden() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getSecretName() {
		// TODO Auto-generated method stub
		return "Kit of Swag";
	}

	@Override
	public String getSecretDescription() {
		// TODO Auto-generated method stub
		return "Thy kit of secretness\nWill be secret until unlock";
	}

	@Override
	public String getSecretCost() {
		// TODO Auto-generated method stub
		return "To unlock, thy must find the key";
	}

}
