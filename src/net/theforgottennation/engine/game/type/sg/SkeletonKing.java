package net.theforgottennation.engine.game.type.sg;


import java.util.ArrayList;

import net.forgottennation.core.cooldown.Cooldown;
import net.forgottennation.core.player.Rank;
import net.forgottennation.core.player.punishment.PunishmentGUI;
import net.theforgottennation.engine.Engine;
import net.theforgottennation.engine.kit.IKitPurchaseable;
import net.theforgottennation.engine.kit.Kit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SkeletonKing extends Kit implements IKitPurchaseable {
	public ArrayList<Block> traps = new ArrayList<>();
	
	public SkeletonKing() {
		super("Skeleton King", Material.SKULL_ITEM, new String[] { "Using the power of skeleton king,", "You can summon minions at your will." });
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCost() {
		// TODO Auto-generated method stub
		return 10000;
	}

	@Override
	public Rank getPerm() {
		// TODO Auto-generated method stub
		return Rank.MYSTIC;
	}

	@Override
	public void GiveItems(Player p) {
		p.getInventory().addItem(new ItemStack(Material.BONE));
		p.getInventory().addItem(new ItemStack(Material.CHEST, 3));
		p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1500, 2));
		p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 1500, 4));
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void placeEvent(BlockPlaceEvent event)
	{
		if(Engine.getInstance().getKitManager().getKit(event.getPlayer()) instanceof SkeletonKing)
		{
			if(event.getBlock().getType() == Material.CHEST)
			{
				traps.add(event.getBlock());
				event.setCancelled(false);
			}
		}
	}
	
	@EventHandler
	public void onChestOpen(PlayerInteractEvent event)
	{
		if(event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
		if(!traps.contains(event.getClickedBlock())) return;
		if(Cooldown.cool(event.getPlayer(), "Chest Trap", 60000)) return;
		Block b = event.getClickedBlock();
		
		event.setCancelled(true);
		b.setType(Material.AIR);
		traps.remove(b);
		
		b.getWorld().strikeLightningEffect(b.getLocation().add(0.5,0,0.5));
		
		
		for(int i = 0 ; i < 10 ; i++){
			 
			Skeleton s;
			if(Math.random() > 0.5)
			{
				s = (Skeleton) b.getWorld().spawnEntity(b.getLocation().add(Math.random(), 3, Math.random()),EntityType.SKELETON);
			} else {
				s = (Skeleton) b.getWorld().spawnEntity(b.getLocation().add(Math.random(), 3, Math.random()),EntityType.SKELETON);
			}
			s.getEquipment().setHelmet(PunishmentGUI.getItem(ChatColor.RESET + "Hunter Helmet", Material.DIAMOND_HELMET, null));
			s.setCustomName(ChatColor.GRAY + event.getPlayer().getName() + "'s Hunter");
		}
		
	}
	
	@EventHandler
	public void onTarget(EntityTargetLivingEntityEvent event)
	{
		for(Player p : Bukkit.getOnlinePlayers())
		{
			if(event.getEntity().getCustomName().equals(ChatColor.GRAY + p.getName() + "'s Hunter"))
			{
				if(p == event.getTarget()) return;
				event.setTarget(p);
			}
		}
	}
}
