package net.theforgottennation.engine.game.type.sg;

import java.util.Random;

import net.forgottennation.core.player.FPlayer;
import net.forgottennation.core.scoreboard.PlayerScoreboard;
import net.theforgottennation.engine.Engine;
import net.theforgottennation.engine.game.Game;
import net.theforgottennation.engine.game.GamePlayer;
import net.theforgottennation.engine.game.PlayerType;
import net.theforgottennation.engine.game.PreventionSet;
import net.theforgottennation.engine.map.MapManager;
import net.theforgottennation.engine.teams.GameTeam;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class SurvivalGames extends Game{

	public SurvivalGames() {
		PreventionSet set = this.getPreventionSet();
		
		set.pve = true;
		set.pvp = true;
		set.canMoveDuringPreGame = false;
		set.deathmatch = true;
		set.waterDamage = true;
		set.maxPlayers = 4;
		set.minPlayers = 1;
		set.placeBlock = true;
		set.breakBlock = true;
		set.timeLimit = true;
		set.timeLimitSec = 600;
		// TODO Auto-generated constructor stub
		
		kits.add(new DefaultKit());
		kits.add(new SkeletonKing());
		kits.add(new BusWankerKit());
	
		teams.add(new GameTeam("Players", ChatColor.YELLOW));
		
	}


	@Override
	public void onGameStart() {
		World w = MapManager.getCurrentMap().World;
		
		for(Chunk chunk : w.getLoadedChunks())
		{
			for(BlockState state : chunk.getTileEntities())
			{
				if(state instanceof Chest)
				{
					Chest chest = (Chest) state;
					fillChest(chest.getBlockInventory());
				}
			}
		}
		
	}
	
	public void fillChest(Inventory inv){
		inv.clear();
		int i = 0;
		for(@SuppressWarnings("unused") ItemStack it :  inv.getContents()){
		Random r = new Random();
		 
		ItemStack item = new ItemStack(Material.AIR);
		int random = r.nextInt(200);
		if( 50 <= random){
			item = new ItemStack(Material.AIR);
		} else 
		if(random <= 7)
		{
			int type = r.nextInt(9);
			switch(type)
			{
			case 0:
				item = new ItemStack(Material.DIAMOND_SWORD);
				break;
			case 1:
				item = new ItemStack(Material.IRON_SWORD);
				break;
			case 2: case 9:
				item = new ItemStack(Material.GOLD_SWORD);
				break;
			case 3: case 7: case 8:
				item = new ItemStack(Material.STONE_SWORD);
			case 4: case 5: case 6: 
				item = new ItemStack(Material.WOOD_SWORD);
			}
		} else
		if(random <= 15)
		{
			item = new ItemStack(Material.COOKED_CHICKEN,3);
		} else
		if(random <= 25)
		{
			int type = r.nextInt(4);
			switch(type)
			{
			case 0:
				item = new ItemStack(Material.DIAMOND_AXE);
				break;
			case 1:
				item = new ItemStack(Material.IRON_AXE);
				break;
			case 2:
				item = new ItemStack(Material.GOLD_AXE);
				break;
			case 3:
				item = new ItemStack(Material.STONE_AXE);
			case 4: 
				item = new ItemStack(Material.WOOD_AXE);
			}
		}
		else if(random <= 30){
			item = new ItemStack(Material.ARROW);
		}
		else if(random <= 40)
		{
			int armorType = r.nextInt(3);
			int value = r.nextInt(9);
			
			switch(armorType)
			{
			case 0:
				switch(value)
				{
				case 0:
					item = new ItemStack(Material.DIAMOND_HELMET);
					break;
				case 1: case 2:
					item = new ItemStack(Material.IRON_HELMET);
					break;
				case 3: case 4: case 5: case 9:
					item = new ItemStack(Material.GOLD_HELMET);
					break;
				case 6: case 7: case 8:
					item = new ItemStack(Material.CHAINMAIL_HELMET);
					break;
				}
				break;
			case 1:
				switch(value)
				{
				case 0:
					item = new ItemStack(Material.DIAMOND_CHESTPLATE);
					break;
				case 1: case 2:
					item = new ItemStack(Material.IRON_CHESTPLATE);
					break;
				case 3: case 4: case 5: case 9:
					item = new ItemStack(Material.GOLD_CHESTPLATE);
					break;
				case 6: case 7: case 8:
					item = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
					break;
				}
				break;
			case 2:
				switch(value)
				{
				case 0:
					item = new ItemStack(Material.DIAMOND_LEGGINGS);
					break;
				case 1: case 2:
					item = new ItemStack(Material.IRON_LEGGINGS);
					break;
				case 3: case 4: case 5: case 9:
					item = new ItemStack(Material.GOLD_LEGGINGS);
					break;
				case 6: case 7: case 8:
					item = new ItemStack(Material.CHAINMAIL_LEGGINGS);
					break;
				}
				break;
			case 3:
				switch(value)
				{
				case 0:
					item = new ItemStack(Material.DIAMOND_BOOTS);
					break;
				case 1: case 2:
					item = new ItemStack(Material.IRON_BOOTS);
					break;
				case 3: case 4: case 5: case 9:
					item = new ItemStack(Material.GOLD_BOOTS);
					break;
				case 6: case 7: case 8:
					item = new ItemStack(Material.CHAINMAIL_BOOTS);
					break;
				}
				break;
			}
		}
		inv.setItem(i, item);
		i++;
		}
	}
	
	@Override
	public void onPreGameStart() {
		// TODO Auto-generated method stub
		
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageByEntityEvent event)
	{
		if(!(event.getDamager() instanceof Player)) return;
		Player player = (Player) event.getDamager();
		
		for(GamePlayer p : getPlayersWithType(PlayerType.DEAD))
		{
			if(p.player.getPlayer() == player.getPlayer()) event.setCancelled(true);
			
			
		}
	}
	
	
	@Override
	public void handleSidebar(PlayerScoreboard sidebar,Player owner) {
		sidebar.setLine(16,ChatColor.GOLD + "" + ChatColor.BOLD + "SG DEV TEST");
		
		int num = 0;
		for(GamePlayer gp : Engine.getInstance().getGameManager().getPlayers())
		{
			if(gp.type ==PlayerType.ALIVE) num ++;
		}
		sidebar.setLine(15," ");
		sidebar.setLine(14,ChatColor.GOLD + "" + ChatColor.BOLD + "Tributes:");
		sidebar.setLine(13,num + "");
		
		sidebar.setLine(12,ChatColor.RESET + " ");
		
		sidebar.setLine(11,ChatColor.RED + "" + ChatColor.BOLD + "Time Left:");
		if(!hasDeathmatch()){
			int clock = Engine.getInstance().getGameManager().getTimer();
			int sec = clock % 60;
			int min = (clock-sec)/60;
			
			sidebar.setLine(10,min + ":" + sec);
		} else {
			sidebar.setLine(10,ChatColor.BOLD + "DEATHMATCH");
		}
	}

	@Override
	public void onGameSecond() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onTimeLimitExceed() {
		
	}


	@Override
	public void onGameEnd() {
		// TODO Auto-generated method stub
		
	}


}
