package net.theforgottennation.engine.game.type.sg;

import net.theforgottennation.engine.kit.Kit;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class DefaultKit extends Kit{

	public DefaultKit() {
		
		super("Player", Material.BONE, new String[] { "The default Player kit" });
		// TODO Auto-generated constructor stub
	}

	@Override
	public void GiveItems(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1500, 2));
		p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 1500, 4));
	}
	
}
