package net.theforgottennation.engine.game;

import java.util.Arrays;
import java.util.List;

import net.md_5.bungee.api.ChatColor;
import net.theforgottennation.engine.game.type.dtc.DefendTheCore;
import net.theforgottennation.engine.game.type.sg.SurvivalGames;
import net.theforgottennation.engine.game.type.skywars.SkyWars;
import net.theforgottennation.engine.game.type.tdm.TeamDeathmatch;
import net.theforgottennation.engine.game.type.uhc.UHC;

public enum GameType {
	SKYWARS("SkyWars", Arrays.asList("Welcome to SkyWars, one of the most known minigames.", "Start by gathering resources, Then build to other islands!", "With the resources you have gathered, eliminate the enemy players!"),new SkyWars()),
	SG("Survival Games", Arrays.asList(ChatColor.RED + "Deprecated", "Survival Games"), new SurvivalGames()),
	UHC("UHC", Arrays.asList(ChatColor.RED + "Deprecated","Ultra Hardcore", "Survive"), new UHC() ),
	DEATHMATCH("Deathmatch",Arrays.asList("Classic Deathmatch", "First to 100 kills wins!"), new TeamDeathmatch()),
	DTC("Defend The Core", Arrays.asList("Welcome to DTC, a more competetive minigame.", "Start by building across the enemy generators and destroying them (by holding right click)", "Then, attack the enemy core.", "Don't forgot to defend your core!"), new DefendTheCore());
	
	String name;
	List<String> desc;
	Game instance;
	
	private GameType(String name, List<String> desc, Game instance) {
		this.name = name;
		this.desc = desc;
		this.instance = instance;
	}

	public String getName() {
		return name;
	}

	public List<String> getDesc() {
		return desc;
	}

	public Game getInstance() {
		return instance;
	}

	
}
