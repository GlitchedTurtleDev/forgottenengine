package net.theforgottennation.engine.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import net.forgottennation.core.Core;
import net.forgottennation.core.listeners.FListener;
import net.forgottennation.core.player.FPlayer;
import net.forgottennation.core.player.punishment.PunishmentGUI;
import net.forgottennation.core.scoreboard.PlayerScoreboard;
import net.forgottennation.core.utils.UtilPrefix;
import net.md_5.bungee.api.ChatColor;
import net.theforgottennation.engine.Engine;
import net.theforgottennation.engine.game.damage.DamageHandler;
import net.theforgottennation.engine.game.damage.DamageValidator;
import net.theforgottennation.engine.game.dm.Deathmatch;
import net.theforgottennation.engine.game.event.GameEndEvent;
import net.theforgottennation.engine.game.event.PlayerKillEvent;
import net.theforgottennation.engine.kit.Kit;
import net.theforgottennation.engine.managers.CoinManager;
import net.theforgottennation.engine.managers.CoinReason;
import net.theforgottennation.engine.teams.GameTeam;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockCanBuildEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class Game extends FListener{
	public abstract void onGameStart();
	public abstract void onPreGameStart();
	public abstract void onGameSecond();
	public abstract void onTimeLimitExceed();
	public abstract void onGameEnd();
	public abstract void handleSidebar(PlayerScoreboard s, Player owner);
	
	private Deathmatch dm;
	protected ArrayList<GameTeam> teams = new ArrayList<>();
	protected ArrayList<Kit> kits = new ArrayList<>();
	private PreventionSet ps = new PreventionSet();
	
	public boolean hasDeathmatch()
	{ 
		if(!ps.deathmatch) return false;
		
		return dm != null;
		
	}
	
	public void assignTeams()
	{
		int numOfTeams = teams.size();
		int clock = 0;
		for(GamePlayer gp : getPlayers())
		{
			gp.team = teams.get(clock);
			clock++;
			if(clock >= numOfTeams)
			{
				clock = 0;
			}
		}
	}
	
	public ArrayList<GameTeam> getTeams() {
		return teams;
	}
	public void startDeathmatch()
	{
		if(hasDeathmatch()) return;
		
		dm = new Deathmatch();
		
	}
	
	public Deathmatch getDeathmatch()
	{
		return dm;
	}
	
	
	public void registerListeners()
	{
		this.register();
	}
	
	public ArrayList<Kit> getKits() {
		return kits;
	}
	public PreventionSet getPreventionSet()
	{
		return ps;
	}
	
	public int getAlivePlayers()
	{
		int i = 0;
		for(GamePlayer p : Engine.getInstance().getGameManager().getPlayers())
		{
			if(p.type == PlayerType.ALIVE) i++;
		}
		return i;
	}
	
	public ArrayList<GamePlayer> getPlayers()
	{
		return Engine.getInstance().getGameManager().getPlayers();
	}
	
	public ArrayList<GamePlayer> getPlayersWithType(PlayerType type)
	{
		ArrayList<GamePlayer> players = new ArrayList<>();
		for(GamePlayer player : getPlayers()){
			if(player.type == type) players.add(player);
		}
		return players;
	}
	
	public ArrayList<GamePlayer> getGamePlayerByPlayer(FPlayer play)
	{
		ArrayList<GamePlayer> players = new ArrayList<>();
		for(GamePlayer player : getPlayers()){
			if(player.player == play) players.add(player);
		}
		return players;
	}
	
	public ArrayList<GamePlayer> getGamePlayerByPlayer(Player play)
	{
		ArrayList<GamePlayer> players = new ArrayList<>();
		for(GamePlayer player : getPlayers()){
			if(player.player.getPlayer().getPlayer() == play) players.add(player);
		}
		return players;
	}
	
	public void killPlayer(FPlayer p)
	{
		Player play = p.getPlayer();
		UUID highest = getGamePlayerByPlayer(p).get(0).getLastDamager();
		String weapon = getGamePlayerByPlayer(p).get(0).getLastDamageWith();
		if(highest != null){
			if(Bukkit.getPlayer(highest) != null)
			{
				Bukkit.broadcastMessage(UtilPrefix.getPrefix("Combat") + ChatColor.YELLOW + play.getDisplayName() + ChatColor.RESET + 
						" was killed by " + ChatColor.YELLOW + Bukkit.getPlayer(highest).getDisplayName() + ChatColor.RESET + " with " + ChatColor.YELLOW + weapon);
				Bukkit.getPluginManager().callEvent(new PlayerKillEvent(Bukkit.getPlayer(highest), play));	
				getGamePlayerByPlayer(Bukkit.getPlayer(highest)).get(0).kills += 1;
			}
		} else if(weapon != null){ 
			Bukkit.broadcastMessage(UtilPrefix.getPrefix("Combat") + ChatColor.YELLOW + play.getDisplayName() + ChatColor.RESET + 
					" was killed by " + ChatColor.YELLOW + weapon);
		} else {
			Bukkit.broadcastMessage(UtilPrefix.getPrefix("Combat") + ChatColor.YELLOW + play.getDisplayName() + ChatColor.RESET + 
					" was killed by " + ChatColor.YELLOW + "their own stupidity");
		}
		
		if(getPreventionSet().teamGame && !getPreventionSet().respawn)
		{
			killPlayerTeam(p);
			return;
		}
		if(getPreventionSet().respawn)
		{
			killPlayerRespawn(p.getPlayer());
			return;
		}
		killPlayerNormal(p);
	}
	
	public void removePlayer(FPlayer p)
	{
		Engine.getInstance().getGameManager().removePlayer(getGamePlayerByPlayer(p).get(0));
		
		if(!getPreventionSet().teamGame || !getPreventionSet().respawn)
		{
			if(getAlivePlayers() == 1)
			{
				endGame(getPlayersWithType(PlayerType.ALIVE).get(0).player.getPlayer(),null,null);
				return;
			}
			if(getAlivePlayers() < 1)
			{
				endGame(null,null,null) ;
			}
		}
	}
	public void killPlayerNormal(FPlayer p)
	{
		Player play = p.getPlayer();
		
		for(GamePlayer pl : getGamePlayerByPlayer(play)){
			pl.type = PlayerType.DEAD;
		}
		
		
		play.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 100, false,false));
		play.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "YOU HAVE DIED! " + ChatColor.GOLD + "" + ChatColor.BOLD + "BUT DON'T WORRY! THE GAME WILL END SOON");
		play.getInventory().clear();
		
		play.getInventory().setItem(0, PunishmentGUI.getItem(ChatColor.GOLD + "Magic Compass", Material.COMPASS, Arrays.asList(ChatColor.RESET + "A way to stalk the admins in the game!")));
		play.setCanPickupItems(false);
		
		play.updateInventory();
		for(GamePlayer pl : getGamePlayerByPlayer(p)){
			pl.type = PlayerType.DEAD;
		}
		for(Player pl : Bukkit.getOnlinePlayers())
		{
			if(play == pl) continue;
			pl.hidePlayer(play);
		}
		if(getAlivePlayers() == 1)
		{
			endGame(getPlayersWithType(PlayerType.ALIVE).get(0).player.getPlayer(), p.getPlayer(),null);
			return;
		}
		if(getAlivePlayers() < 1)
		{
			endGame(null,p.getPlayer(),null) ;
		}
	}
	
	public void killPlayerRespawn(final Player play)
	{
		play.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 100, false,false));
		play.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "YOU HAVE DIED! " + ChatColor.GOLD + "" + ChatColor.BOLD + "You will respawn in 3 seconds");
		play.getInventory().clear();
		
		for(GamePlayer pl : getGamePlayerByPlayer(play)){
			pl.type = PlayerType.DEAD;
		}
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if(getGamePlayerByPlayer(play).get(0).type == PlayerType.ALIVE)
					return;
				
				for(GamePlayer pl : getGamePlayerByPlayer(play)){
					pl.type = PlayerType.ALIVE;
				}
				play.sendMessage(ChatColor.BLUE + "Respawn >>" + ChatColor.RESET + "You have respawned!");
				play.setFlying(false);
				play.setAllowFlight(false);
				play.setFireTicks(0);
				play.setFallDistance(0);
				Engine.getInstance().getKitManager().getKit(play).GiveItems(play);
				play.teleport(getGamePlayerByPlayer(play).get(0).team.spawns.get(0));
			}
		}.runTaskLater(Core.getInstance(), 60);
	}
	
	public void killPlayerTeam(FPlayer p)
	{
		for(GamePlayer pl : getGamePlayerByPlayer(p)){
			pl.type = PlayerType.DEAD;
		}
		Player play = Bukkit.getPlayer(p.getPlayer().getName());
		

		play.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 100, false,false));
		play.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "YOU HAVE DIED! " + ChatColor.GOLD + "" + ChatColor.BOLD + "BUT DON'T WORRY! THE GAME WILL END SOON");
		play.getInventory().clear();
		
		play.getInventory().setItem(0, PunishmentGUI.getItem(ChatColor.GOLD + "Magic Compass", Material.COMPASS, Arrays.asList(ChatColor.RESET + "A way to stalk the admins in the game!")));
		play.setCanPickupItems(false);
		
		play.updateInventory();
		for(Player pl : Bukkit.getOnlinePlayers())
		{
			if(play == pl) continue;
			pl.hidePlayer(play);
		}
		GamePlayer gp = getGamePlayerByPlayer(play).get(0);
		if(gp.team.getAlivePlayers() == 0)
		{
			for(GameTeam t : teams)
			{
				if(t.getAlivePlayers() != 0)
				{
					endGameTeams(t);
				}
			}
		}
	}
	
	// Solo games.
	public void endGame(OfflinePlayer winner, OfflinePlayer second, OfflinePlayer third)
	{
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "===============================");
		Bukkit.broadcastMessage(" ");
		Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "The game has Ended!");
		Bukkit.broadcastMessage(" ");
		if(winner != null){
			Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "1st Place: " + winner.getPlayer().getDisplayName());
		}
		if(second != null){
			Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "2nd Place: " + second.getPlayer().getDisplayName());
		}
		if(third != null){
			Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "3rd Place: " + third.getPlayer().getDisplayName());
		}
		
		for(Player p : Bukkit.getOnlinePlayers())
		{
			GamePlayer gp = getGamePlayerByPlayer(FPlayer.getPlayer(p)).size() == 1 ? getGamePlayerByPlayer(FPlayer.getPlayer(p)).get(0) : null;
			if(gp == null) continue;
			
			List<CoinReason> reasons = new ArrayList<>();
			if(winner == p) reasons.add(new CoinReason("First Place", 45, 250));
			if(second == p) reasons.add(new CoinReason("Second Place", 25, 150));
			if(third == p) reasons.add(new CoinReason("Third Place", 10, 75));
			reasons.add(new CoinReason(gp.kills + " kills", gp.kills * 5, gp.kills * 10));
			reasons.add(new CoinReason("Participation", 10,75));
			CoinManager.GiveMoney(p, reasons);
		}
		
		Bukkit.broadcastMessage(" ");
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "===============================");
		endGame();
	}
	
	// Team Games
	public void endGameTeams(GameTeam winner)
	{
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "===============================");
		Bukkit.broadcastMessage(" ");
		Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "The game has Ended!");
		Bukkit.broadcastMessage(" ");
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + winner.name + " has won the Game!");
		
		for(Player p : Bukkit.getOnlinePlayers())
		{
			GamePlayer gp = getGamePlayerByPlayer(FPlayer.getPlayer(p)).size() == 1 ? getGamePlayerByPlayer(FPlayer.getPlayer(p)).get(0) : null;
			if(gp == null) continue;
			
			List<CoinReason> reasons = new ArrayList<>();
			if(gp.team == winner) reasons.add(new CoinReason("Winning Team", 45, 100));
			reasons.add(new CoinReason(gp.kills + " kills", gp.kills * 2, gp.kills * 6));
			reasons.add(new CoinReason("Participation", 10,10));
			CoinManager.GiveMoney(p, reasons);
		}
		
		Bukkit.broadcastMessage(" ");
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "===============================");
		endGame();
	}
	
	// Other Game...
	public void endGame(String winner)
	{
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "===============================");
		Bukkit.broadcastMessage(" ");
		Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "The game has Ended!");
		Bukkit.broadcastMessage(" ");
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + winner + " has won the Game!");
		
		for(Player p : Bukkit.getOnlinePlayers())
		{
			GamePlayer gp = getGamePlayerByPlayer(FPlayer.getPlayer(p)).size() == 1 ? getGamePlayerByPlayer(FPlayer.getPlayer(p)).get(0) : null;
			if(gp == null) continue;
			
			List<CoinReason> reasons = new ArrayList<>();
			reasons.add(new CoinReason(gp.kills + " kills", gp.kills * 2, gp.kills * 6));
			reasons.add(new CoinReason("Participation", 10,10));
			CoinManager.GiveMoney(p, reasons);
		}
		
		Bukkit.broadcastMessage(" ");
		Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "===============================");
		endGame();
	}
	
	// Called to end the game with no winners etc.
	public void endGame()
	{
		this.onGameEnd();
		new BukkitRunnable() {
			
			@Override
			public void run() {
				for(Player p : Bukkit.getOnlinePlayers())
				{
					CoinManager.GiveLoot(p);
				}
				
			}
		}.runTaskLater(Core.getInstance(), 80);
		Bukkit.getPluginManager().callEvent(new GameEndEvent(Engine.getInstance().getGameManager().getGame()));
		this.unregister();
		Engine.getInstance().getGameManager().stopGame();
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event)
	{
		if(this.getGamePlayerByPlayer(FPlayer.getPlayer(event.getPlayer())).get(0).type != PlayerType.ALIVE)
		{
			event.setCancelled(true);
		}
	}
	
	/*@EventHandler(priority = EventPriority.MONITOR)
	public void onDamage(EntityDamageEvent event)
	{
		if(!(event.getEntity() instanceof Player))
			return;
		if(Engine.getInstance().getGameManager().getGame().getInstance()
			.getGamePlayerByPlayer(FPlayer.getPlayer((Player) event.getEntity())).size() == 0) return;
		
		if(!(event.getCause() == DamageCause.ENTITY_ATTACK))
		{
			Engine.getInstance().getGameManager().getGame().getInstance()
			.getGamePlayerByPlayer(FPlayer.getPlayer((Player) event.getEntity())).get(0).damage(new CombatToken(event.getCause().toString(), event.getFinalDamage()));	
		}
		
	}*/
	
	@EventHandler
	public void onBreak(BlockBreakEvent event)
	{
		if(this.getGamePlayerByPlayer(FPlayer.getPlayer(event.getPlayer())).get(0).type != PlayerType.ALIVE)
		{
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBreak(BlockPlaceEvent event)
	{
		if(this.getGamePlayerByPlayer(FPlayer.getPlayer(event.getPlayer())).get(0).type != PlayerType.ALIVE)
		{
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent event)
	{
		if(this.getGamePlayerByPlayer(event.getPlayer()).get(0).type != PlayerType.ALIVE)
		{
			event.setCancelled(true);
		}
		
	}
	
	@EventHandler
	public void onDrop(PlayerPickupItemEvent event)
	{
		if(this.getGamePlayerByPlayer(event.getPlayer()).get(0).type != PlayerType.ALIVE)
		{
			event.setCancelled(true);
		}
		
	}
	
	@EventHandler(priority = EventPriority.LOW)
    public void onBlockCanBuildEvent(BlockCanBuildEvent e){
        e.setBuildable(true);
    }
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEnDamKill(EntityDamageEvent event)
	{
		if(!(event.getEntity() instanceof Player)) 
			return;
		Player player = (Player) event.getEntity();
		
		boolean validAttack = DamageValidator.isValidAttack(event);
		
		if(!validAttack)
		{
			event.setCancelled(true);
			return;
		}
		
		DamageHandler.logAttack(event);
		
		if(player.getHealth()-event.getFinalDamage() > 1)
			return;
		
		if(this.getPreventionSet().dropItems)
			DamageHandler.dropItems(player);
		
		this.getGamePlayerByPlayer(player).get(0).revokeDamage();
		
		killPlayer(FPlayer.getPlayer(player));
		event.setCancelled(true);
	}
}
