package net.theforgottennation.engine.game.event;

import net.theforgottennation.engine.game.GameType;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameEndEvent extends Event{
	private static final HandlerList handlers = new HandlerList();
	private  GameType type;
	@Override
	public HandlerList getHandlers() 
	{
		// TODO Auto-generated method stub
		return handlers;
	}
	
	public static HandlerList getHandlerList() 
	{
		// TODO Auto-generated method stub
		return handlers;
	}
	
	public GameEndEvent(GameType type) {
		// TODO Auto-generated constructor stub
		this.type = type;
	}

	public GameType getType() {
		return type;
	}
	
	
}
